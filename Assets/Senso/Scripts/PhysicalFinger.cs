﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicalFinger : MonoBehaviour
{
    public Senso.EFingerType Finger;
    public Senso.EPositionType Hand;
    public SensoHandsController HandsController;
    public LayerMask IgnoreLayer;
    private void OnTriggerStay(Collider collision)
    {
        if(collision.gameObject.layer != IgnoreLayer)
            HandsController.SendVibro(Hand, Finger, "1");
    }
}
