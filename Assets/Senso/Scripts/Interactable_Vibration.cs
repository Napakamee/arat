﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable_Vibration : MonoBehaviour
{
    public int VibrationID = 2;
    private ushort duration = 0;
    public bool VibrateOnce;
    public bool FixedVibration = true;

    bool Vibrated;
    GameObject InteractableObject;
    InteractableJoint InteractableJoint;

    public void Start()
    {
        InteractableObject = this.gameObject;
        InteractableJoint = InteractableObject.GetComponent<InteractableJoint>();
    }

    public void GrabVibrate(ushort duration, int Hardness)
    {
        InteractableJoint.SensoHandExample.VibrateFinger(Senso.EFingerType.Thumb, duration, Hardness.ToString());
        InteractableJoint.SensoHandExample.VibrateFinger(Senso.EFingerType.Index, duration, Hardness.ToString());
        InteractableJoint.SensoHandExample.VibrateFinger(Senso.EFingerType.Third, duration, Hardness.ToString());
        InteractableJoint.SensoHandExample.VibrateFinger(Senso.EFingerType.Middle, duration, Hardness.ToString());
        InteractableJoint.SensoHandExample.VibrateFinger(Senso.EFingerType.Little, duration, Hardness.ToString());
    }

    public void PinchVibrate(ushort duration, int Hardness)
    {
        InteractableJoint.SensoHandExample.VibrateFinger(Senso.EFingerType.Thumb, duration, Hardness.ToString());
        InteractableJoint.SensoHandExample.VibrateFinger(Senso.EFingerType.Index, duration, Hardness.ToString());
    }

    void Update()
    {
        if (InteractableJoint.gesture != null)
        {
            if (InteractableJoint.Grab)
            {
                if (FixedVibration == false)
                    VibrationID = InteractableJoint.gesture.VibrationID;

                if (InteractableJoint.Grabbed && VibrateOnce && !Vibrated)
                {
                    GrabVibrate(duration, VibrationID);
                    Vibrated = true;
                }

                else if (InteractableJoint.Grabbed && !VibrateOnce)
                    GrabVibrate(duration, VibrationID);

                else if (!InteractableJoint.Grabbed)
                    Vibrated = false;
            }

            else if (InteractableJoint.Pinch)
            {
                if (InteractableJoint.Pinched && VibrateOnce && !Vibrated)
                {
                    PinchVibrate(duration, VibrationID);
                    Vibrated = true;
                }

                else if (InteractableJoint.Pinched && !VibrateOnce)
                    PinchVibrate(duration, VibrationID);

                else if (!InteractableJoint.Pinched)
                    Vibrated = false;
            }
        }
    }

}
