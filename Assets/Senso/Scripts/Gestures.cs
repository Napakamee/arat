﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gestures : MonoBehaviour
{

    public SensoHandExample SensoHandExample;
    public Senso.HandData HandData;
    public int hard;
    public int VibrationID;
    public GameObject SphereCollider;
    public GameObject[] FingerColliders;
    public GameObject[] PhysicalThumbs;
    public GameObject[] PhysicalIndex;
    public GameObject[] PhysicalMiddle;
    public GameObject[] PhysicalThird;
    public GameObject[] PhysicalLittle;
    public LayerMask IgnoreLayer;
    public Senso.EPositionType Hand;
    private float hardness;
    private SensoHandsController Controller;
    float hardnesschar;
    Rigidbody rb;
    [HideInInspector]public bool PinchedOrGrabbed = false;
    [Header("Gesture")]
    public bool grab;
    public bool pinch;
    public bool Vgesture;

    void Start()
    {
        Controller = FindObjectOfType<SensoHandsController>();
        SphereCollider.SetActive(false);
        if(SensoHandExample == null)
            SensoHandExample = this.gameObject.GetComponent<SensoHandExample>();
        for (int i = 0; i < 5; i++)
        {
            GameObject[] arr;
            switch (i)
            {
                case 1: arr = PhysicalIndex; break;
                case 2: arr = PhysicalMiddle; break;
                case 3: arr = PhysicalThird; break;
                case 4: arr = PhysicalLittle; break;
                default: arr = PhysicalThumbs; break;
            }

            for (int j = 0; j < arr.Length; ++j)
            {
                PhysicalFinger finger = arr[j].AddComponent<PhysicalFinger>();
                finger.Finger = (Senso.EFingerType)Enum.ToObject(typeof(Senso.EFingerType), i);
                finger.Hand = Hand;
                finger.HandsController = Controller;
                finger.IgnoreLayer = IgnoreLayer;
            }
            
        }
    }

    void Update()
    {
        GrabGesture();
        if(!grab)
            PinchGesture();
        VGesture();
    }

    public void GrabGesture()
    {
        if (SensoHandExample.indexBones[2].localRotation.x <= -0.21 && SensoHandExample.thirdBones[2].localRotation.x <= -0.21 && SensoHandExample.middleBones[2].localRotation.x <= -0.21 && SensoHandExample.littleBones[2].localRotation.x <= 0.21)
        {
            SphereCollider.SetActive(true);
            grab = true;
            hardness = Mathf.Abs((SensoHandExample.indexBones[2].localRotation.x + SensoHandExample.thirdBones[2].localRotation.x + SensoHandExample.middleBones[2].localRotation.x + SensoHandExample.littleBones[2].localRotation.x) / 4);
        }

        else
        {
            if (SphereCollider.TryGetComponent<Rigidbody>(out rb))
                Destroy(rb);
            SphereCollider.SetActive(false);
            grab = false;
        }

        hard = (int)(hardness * 10);
        if (hard < 2)
            VibrationID = 51;
        else if (hard <= 4 && hard > 2)
            VibrationID = 50;
        else if (hard <= 6 && hard > 4)
            VibrationID = 49;
        else if (hard <= 8 && hard > 6)
            VibrationID = 48;
        else if (hard <= 10 && hard > 8)
            VibrationID = 47;
        else
            VibrationID = 51;
    }

    public void PinchGesture()
    {
        if (((SensoHandExample.thumbBones[0].localRotation.y <= -0.07 && SensoHandExample.indexBones[2].localRotation.x <= -0.31)
            || (SensoHandExample.middleBones[2].localRotation.x <= -0.5 && SensoHandExample.thumbBones[0].localRotation.y <= -0.10)
            || (SensoHandExample.thirdBones[2].localRotation.x <= -0.42 && SensoHandExample.thumbBones[0].localRotation.y <= -0.10))
            || (SensoHandExample.indexBones[2].localRotation.x <= -0.25 && SensoHandExample.middleBones[0].localRotation.x <= -0.25) 
            || (SensoHandExample.middleBones[2].localRotation.x <= -0.45 && SensoHandExample.thumbBones[0].localRotation.y <= -0.15 &&
            SensoHandExample.indexBones[2].localRotation.x <= -0.45) && (!grab))
        {
            pinch = true;
            foreach (GameObject obj in FingerColliders)
                obj.SetActive(true);
        }

        else
        {
            pinch = false;
            foreach (GameObject obj in FingerColliders)
                obj.SetActive(false);
        }
    }

    public void VGesture()
    {
        if (SensoHandExample.thirdBones[2].localRotation.x <= -0.2 &&  SensoHandExample.littleBones[2].localRotation.x <= 0.2 && SensoHandExample.indexBones[2].localRotation.x >= -0.2 && SensoHandExample.middleBones[2].localRotation.x >= -0.2)
            Vgesture = true;

        else
            Vgesture = false;
    }

    


}
