using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandAngle : MonoBehaviour
{
     Quaternion roation;

     float angle;
    /// <summary>
    /// Ŀ��Ƕ�1
    /// </summary>
    public float level_1 = 10;
    /// <summary>
    /// Ŀ��Ƕ�2
    /// </summary>
    public float level_2 = 20;
    /// <summary>
    /// Ŀ��Ƕ�3
    /// </summary>
    public float level_3 = 30;
    /// <summary>
    /// Ĭ����ɫ
    /// </summary>
    public Color unmet=Color.black;
    /// <summary>
    /// ����ʱ����ɫ
    /// </summary>
    public Color satisfy= Color.red;

    public Text[] texts;
    public Image image;
    
    // Start is called before the first frame update
    void Start()
    {
        roation = transform.localRotation;
     
    }
    /// <summary>
    /// �Ƿ�׼������������
    /// </summary>
    bool isPlay=false;
    /// <summary>
    /// ��¼��ǰ���ŵ��Ǹ��㼶���ж���
    /// </summary>
    int level_JiLv;


   
    void Update()
    {
        angle = Quaternion.Angle(transform.localRotation, roation);
        image.fillAmount = angle / level_3;
        if (isPlay)//���Ƕ��Ƿ��˻���
        {
            if (level_JiLv==1&& angle < level_1)
            {
                isPlay = false;
                CancelInvoke("Play");
            }

            if (level_JiLv == 2 && angle < level_2)
            {
                isPlay = false;
                CancelInvoke("Play");
            }

            if (level_JiLv == 3 && angle < level_3)
            {
                isPlay = false;
                CancelInvoke("Play");
            }
          
        }

        if (angle >= level_3)
        {
            if (!isPlay && level_JiLv < 3)
            {
                isPlay = true;
                level_JiLv = 3;
                Invoke("Play", 0.5f);
            }
            if (!isPlay)
            {
                level_JiLv = 3;
            }
            texts[0].color = satisfy;
            texts[1].color = satisfy;
            texts[2].color = satisfy;
        }
        else if (angle >= level_2)
        {
            if (!isPlay && level_JiLv < 2)
            {
                isPlay = true;
                level_JiLv = 2;
                Invoke("Play", 0.5f);
            }
            if (!isPlay)
            {
                level_JiLv = 2;
            }
            texts[0].color = satisfy;
            texts[1].color = satisfy;
            texts[2].color = unmet;
        }
        else if(angle>= level_1)
        {
            if (!isPlay&& level_JiLv < 1)
            {
                isPlay = true;
                level_JiLv = 1;
                Invoke("Play", 0.5f);
            }

            if (!isPlay)
            {
                level_JiLv = 1;
            }
            texts[0].color = satisfy;
            texts[1].color = unmet;
            texts[2].color = unmet;
        }
        else
        {
            level_JiLv = 0;
            texts[0].color = unmet;
            texts[1].color = unmet;
            texts[2].color = unmet;
        }

      
        //image.fillAmount = angle / targetAngle;
        //text.text = (image.fillAmount * 100).ToString("f2") + "%";
    }
    public AudioClip[] clips;
    public void Play()
    {
        GetComponent<AudioSource>().clip = clips[level_JiLv-1];
        GetComponent<AudioSource>().Play();
        isPlay = false;
    }
}
