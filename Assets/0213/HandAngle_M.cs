using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandAngle_M : MonoBehaviour
{
    public HandAngle[] handAngles;
    public GameObject ui;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        foreach (var item in handAngles)
        {
            if (item.image.fillAmount<1)
            {
                ui.SetActive(false);
                return;
            }
        }
        ui.SetActive(true);
    }
}
