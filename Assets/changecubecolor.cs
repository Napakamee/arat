using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changecubecolor : MonoBehaviour
{
    private SkinnedMeshRenderer msRender;
    public SensoHandExample SensoHandExample;
    public Material color1;
    public Material color2;
    public Material color3;
    private float smooth = 1.0f;
    // Start is called before the first frame update
    void Start()
    {
        msRender = GetComponent<SkinnedMeshRenderer>();
        msRender.material.color = Color.white;

    }

    // Update is called once per frame
    void Update()
    {

        if ((SensoHandExample.indexBones[2].localRotation.x <= -0.45 && SensoHandExample.thumbBones[0].localRotation.y <= -0.15))
        {
            msRender.material.color = Color.Lerp(msRender.material.color, Color.green, smooth * Time.deltaTime);/*食指*/
        }
        else if ((SensoHandExample.middleBones[2].localRotation.x <= -0.55 && SensoHandExample.thumbBones[0].localRotation.y <= -0.20))
        {
            msRender.material.color = Color.Lerp(msRender.material.color, Color.green, smooth * Time.deltaTime);/*中指*/
        }
        else if ((SensoHandExample.thirdBones[2].localRotation.x <= -0.47 && SensoHandExample.thumbBones[0].localRotation.y <= -0.25))
        {
            msRender.material.color = Color.Lerp(msRender.material.color, Color.green, smooth * Time.deltaTime);/*无名指*/
        }
        else if ((SensoHandExample.indexBones[2].localRotation.x <= -0.30 && SensoHandExample.thirdBones[2].localRotation.x <= -0.30 &&
            SensoHandExample.middleBones[2].localRotation.x <= -0.30 && SensoHandExample.littleBones[2].localRotation.x <= 0.30))
        {
            msRender.material.color = Color.Lerp(msRender.material.color, Color.green, smooth * Time.deltaTime);/*Grab全指1*/
        }
        else if ((SensoHandExample.indexBones[2].localRotation.x <= -0.35 && SensoHandExample.thirdBones[2].localRotation.x <= -0.45 &&
            SensoHandExample.middleBones[2].localRotation.x <= -0.35 && SensoHandExample.littleBones[2].localRotation.x <= 0.40))
        {
            msRender.material.color = Color.Lerp(msRender.material.color, Color.green, smooth * Time.deltaTime);/*Grab全指0.75*/
        }
        else if ((SensoHandExample.indexBones[2].localRotation.x <= -0.40 && SensoHandExample.thirdBones[2].localRotation.x <= -0.45 &&
            SensoHandExample.middleBones[2].localRotation.x <= -0.40 && SensoHandExample.littleBones[2].localRotation.x <= 0.40))
        {
            msRender.material.color = Color.Lerp(msRender.material.color, Color.green, smooth * Time.deltaTime);/*Grab全指0.50*/
        }
        else if ((SensoHandExample.indexBones[2].localRotation.x <= -0.45 && SensoHandExample.thirdBones[2].localRotation.x <= -0.45 && 
            SensoHandExample.middleBones[2].localRotation.x <= -0.45 && SensoHandExample.littleBones[2].localRotation.x <= 0.55))
        {
            msRender.material.color = Color.Lerp(msRender.material.color, Color.green, smooth * Time.deltaTime);/*Grab全指0.25*/
        }
        else if ((SensoHandExample.middleBones[2].localRotation.x <= -0.45 && SensoHandExample.thumbBones[0].localRotation.y <= -0.15 &&
            SensoHandExample.indexBones[2].localRotation.x <= -0.45))
        {
            msRender.material.color = Color.Lerp(msRender.material.color, Color.green, smooth * Time.deltaTime);/*中指和食指*/
        }
        /*else if ((SensoHandExample.indexBones[2].localRotation.x <= -0.31 && SensoHandExample.thirdBones[2].localRotation.x <= -0.42
            && SensoHandExample.middleBones[2].localRotation.x <= -0.5 && SensoHandExample.littleBones[2].localRotation.x <= 0.21
            && SensoHandExample.thumbBones[0].localRotation.y >= -0.07))
        {
            msRender.material.color = Color.Lerp(msRender.material.color, Color.gray, smooth * Time.deltaTime);/*Hook
        }*/
        else
        {
            msRender.material.color = Color.white;
        }
    }
}
