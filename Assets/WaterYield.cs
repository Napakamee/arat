using LiquidVolumeFX;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// ˮ�����
/// </summary>
public class WaterYield : MonoBehaviour
{
    public LiquidVolume liquidVolume;

    public float criterion = 0.2f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Color color = GetComponent<MeshRenderer>().material.color;
        GetComponent<MeshRenderer>().material.color = new Color(color.r, color.g, color.b,1- (liquidVolume.level - criterion)/(1- criterion));
    }
}
