using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ARATToggle : ARATBase
{
    [SerializeField] private GameObject gameObject;
    [SerializeField] private List<GameObject> instructionObjects;
    [SerializeField] private Transform spawnPoint;

    public override void Enable(bool _value)
    {
        foreach (var _instructionObject in instructionObjects.Where(_instructionObject => _instructionObject))
        {
            _instructionObject.SetActive(_value);
        }
        
        gameObject.SetActive(_value);
    }
}
