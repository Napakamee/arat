using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleLock : MonoBehaviour
{
    [SerializeField] private Button button;

    public void OnToggle(bool _value)
    {
        button.interactable = !_value;
    }
}
