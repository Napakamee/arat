using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class InstructionObjects
{
    public List<GameObject> Objects = new List<GameObject>();
}

public class TestController : MonoBehaviour
{
    [SerializeField] private List<InstructionObjects> instructionObjects = new List<InstructionObjects>();
    [SerializeField] private List<ARATBase> aratObjects;

    public void ShowTest(int _index)
    {
        for (var _i = 0; _i < aratObjects.Count; _i++)
        {
            var _arat = aratObjects[_i];

            _arat.Enable(_i == _index);
        }
    }

    public void ShowInstruction(int _index)
    {
        for (var _i = 0; _i < instructionObjects.Count; _i++)
        {
            var _instructions = instructionObjects[_i];

            foreach (var _instruction in _instructions.Objects)
            {
                _instruction.SetActive(_i == _index);
            }
        }
    }
}
