using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OpeningController : MonoBehaviour
{
    public List<GameObject> stories = new List<GameObject>();
    public float delay = 2f;
    private void Start()
    {
        StartCoroutine(OpeningSequence(() =>
        {
            SceneManager.LoadSceneAsync(1);
        }));
    }

    public IEnumerator OpeningSequence(Action _onComplete)
    {
        for (var _index = 0; _index < stories.Count; _index++)
        {
            //var _story = stories[_index];
            for (var _i = 0; _i < stories.Count; _i++)
            {
                var _story2 = stories[_i];
                _story2.SetActive(_index==_i);
            }

            yield return new WaitForSeconds(delay);
        }
        _onComplete?.Invoke();
    }

}
