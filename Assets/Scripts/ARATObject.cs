using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ARATObject : ARATBase
{
    [SerializeField] private GameObject prefab;
    [SerializeField] private List<GameObject> instructionObjects;
    [SerializeField] private Transform spawnPoint;
    private GameObject obj;
    public override void Enable(bool _value)
    {
        foreach (var _instructionObject in instructionObjects.Where(_instructionObject => _instructionObject))
        {
            _instructionObject.SetActive(_value);
        }
        
        if (obj)
        {
            Destroy(obj);
        }
        
        if (_value)
        {
            SpawnPrefab();
        }
    }

    private void SpawnPrefab()
    {
        obj = Instantiate(prefab);
        obj.SetActive(true);
        obj.transform.position = spawnPoint.transform.position;
        obj.transform.rotation = Quaternion.Euler(Vector3.zero);
    }
}
