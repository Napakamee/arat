using System;
using System.Collections;
using System.Collections.Generic;
using ItSeez3D.AvatarMaker;
using ItSeez3D.AvatarSdk.Core;
using UnityEngine;

public class SkinPartController : MonoBehaviour
{
    [SerializeField] private List<GameObject> objList = new List<GameObject>();

    [SerializeField] private int currentIndex;
    private HashSet<string> triggeredName = new HashSet<string>();
    private const string HAIRCUT_OBJECT_NAME = "ItSeez3D Haircut";
    
    private void Awake()
    {
        triggeredName = new HashSet<string>();
        currentIndex = 0;
    }

    private void OnTriggerEnter(Collider col)
    {
        //Debug.Log(triggeredName.Count);
        //Debug.Log(col.gameObject.name, col.gameObject);
        if (triggeredName.Count < 1)
        {
            currentIndex = (currentIndex + 1) % objList.Count;

            for (var _index = 0; _index < objList.Count; _index++)
            {
                var _obj = objList[_index];

                _obj.SetActive(_index == currentIndex);
            }
        }

        triggeredName.Add(col.name);
    }

    private void OnTriggerExit(Collider other)
    {
        triggeredName.Remove(other.name);
        //Debug.Log(triggeredName.Count);
    }
}
