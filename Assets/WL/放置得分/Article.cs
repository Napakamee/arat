using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// 商品
/// </summary>
public class Article : MonoBehaviour
{
    /// <summary>
    /// 得分
    /// </summary>
     public int score;
    /// <summary>
    /// 音频文件
    /// </summary>
    public AudioClip clip;
    /// <summary>
    /// 是否放置了
    /// </summary>
    public bool isPlace=false;
    /// <summary>
    /// 检测距离
    /// </summary>
    public float distance = 0.2f;
    // Start is called before the first frame update
    void Start()
    {
        isPlace = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isPlace)
        {
            for (int i = 0; i < ArticleManager.initialize.plate.Length; i++)
            {
                if ((transform.position - ArticleManager.initialize.plate[i].transform.position).magnitude < distance)
                {
                    isPlace = true;
                    ArticleManager.initialize.scoreMax[i] += score;
                    GetComponent<AudioSource>().clip = clip;
                    GetComponent<AudioSource>().Play();
                    return;
                }
            }
         
        }
    }
}
