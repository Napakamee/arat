using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ArticleManager : MonoBehaviour
{
    public static ArticleManager initialize;
    private void Awake()
    {
        initialize = this;
    }
    /// <summary>
    /// 总得分
    /// </summary>
    public int[] scoreMax;
    /// <summary>
    /// 文本显示
    /// </summary>
    public TextMeshProUGUI[] textMeshProUGUI;
    /// <summary>
    /// 盘子
    /// </summary>
    public GameObject[] plate;
    void Update()
    {

        for (int i = 0; i < textMeshProUGUI.Length; i++)
        {
            textMeshProUGUI[i].text = scoreMax[i].ToString();
        }
    }
}
