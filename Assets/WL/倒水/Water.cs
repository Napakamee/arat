using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// ˮ
/// </summary>
public class Water : MonoBehaviour
{
    [SerializeField] private float destroySelfIn = 2;
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DestroySelf());
    }

    private IEnumerator DestroySelf()
    {
        yield return new WaitForSeconds(destroySelfIn);
        Destroy(gameObject);
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }
}
